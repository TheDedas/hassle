package com.iscte.it.HASSLE;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Rectangle2D;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.Scanner;
import burlap.domain.singleagent.gridworld.GridWorldVisualizer;
import burlap.mdp.core.StateTransitionProb;
import burlap.mdp.core.TerminalFunction;
import burlap.mdp.core.action.Action;
import burlap.mdp.core.action.UniversalActionType;
import burlap.mdp.core.state.State;
import burlap.mdp.singleagent.SADomain;
import burlap.mdp.singleagent.environment.SimulatedEnvironment;
import burlap.mdp.singleagent.model.FactoredModel;
import burlap.mdp.singleagent.model.RewardFunction;
import burlap.mdp.singleagent.model.statemodel.FullStateModel;
import burlap.shell.visual.VisualExplorer;
import burlap.visualizer.StatePainter;
import burlap.visualizer.StateRenderLayer;
import burlap.visualizer.Visualizer;

public class OfficeGridWorld {
	// map size: 68 x 46
	// inner walls = 148
	// door cells = 12
	// number of orientations = 4
	// number of states stated in the paper = 10973
	// how to reach the number of states stated: (44*66 - 148 - 12 - 1) * 4 + 1
	// steps: sum all free cells minus the goal, multiply that by 4 possible orientations, add the goal state
	public static final String	VAR_X = "x";
	public static final String	VAR_Y = "y";
	public static final String 	VAR_ORIENTATION = "orientation";
	public static final String 	VAR_DIST_FORWARD = "distForward";
	public static final String 	VAR_DIST_RIGHT = "distRight";
	public static final String 	VAR_DIST_BACKWARD = "distBackward";
	public static final String 	VAR_DIST_LEFT = "distLeft";
	public static final String 	VAR_IS_DOOR_FORWARD = "isDoorForward";
	public static final String 	VAR_IS_DOOR_RIGHT = "isDoorRight";
	public static final String 	VAR_IS_DOOR_BACKWARD = "isDoorBackward";
	public static final String 	VAR_IS_DOOR_LEFT = "isDoorLeft";
	public static final String 	VAR_IS_GOAL_FORWARD = "isGoalForward";
	public static final String 	VAR_IS_GOAL_RIGHT = "isGoalRight";
	public static final String 	VAR_IS_GOAL_BACKWARD = "isGoalBackward";
	public static final String 	VAR_IS_GOAL_LEFT = "isGoalLeft";
	
	public static final int 	nActions = 3;
	public static final String 	ACTION_TAKE_STEP = "step";
	public static final String 	ACTION_TURN_RIGHT = "right";
	public static final String 	ACTION_TURN_LEFT = "left";
	
	protected int goalx = 64;
	protected int goaly = 3;
	
	protected int [][] map;
	protected ARAVQ aravq;
	
	protected static final String FILEPATH = System.getProperty("user.dir") + "/res/ArticleMap.txt";
	protected static Random rand = new Random();

	public static void main(String [] args){
		OfficeGridWorld gen = new OfficeGridWorld();
		SADomain domain = gen.generateDomain();
		
		State initialState = randomInitialState(gen.getMap());
		SimulatedEnvironment env = new SimulatedEnvironment(domain, initialState);

		Visualizer v = gen.getVisualizer();
		VisualExplorer exp = new VisualExplorer(domain, env, v);

		exp.addKeyAction("w", ACTION_TAKE_STEP, "");
		exp.addKeyAction("d", ACTION_TURN_RIGHT, "");
		exp.addKeyAction("a", ACTION_TURN_LEFT, "");
		
		exp.initGUI();
	}
	
	public OfficeGridWorld() {
		readMapFromFile(FILEPATH);
		aravq = new ARAVQ(0, Integer.MAX_VALUE, 32, 4, 1);
	}
	
	private int[][] readMapFromFile(String filepath2) {
		try {
			Scanner scanner = new Scanner(new File(FILEPATH));
			List<String> lines = new ArrayList<String>();
			while (scanner.hasNext()) {
				lines.add(scanner.nextLine());
			}
			scanner.close();
			int lineSize = lines.get(0).length();
			int lineNumber = lines.size();
			map = new int[lineSize][lineNumber];
			for(int x = 0; x < lineSize; x++){
				for(int y = 0; y < lineNumber; y++){
					char c = lines.get(lineNumber - 1 - y).charAt(x);
					if(c == 'W'){
						map[x][y] = 1; // for a wall
					}else if(c == 'd'){
						map[x][y] = 9; // for a door
					}else{
						map[x][y] = 0; // for an empty square
					}
				}
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		return map;
	}
	
	public static OfficeGridState randomInitialState(int[][] map){
		int[] initPos = randomStartPosition(map);
		return new OfficeGridState(map,initPos[0], initPos[1], initPos[2]);
	}
	
	public int[][] getMap(){
		return map;
	}
	
	public List<int[]> getHighObservations(){
		return aravq.getModelVectors();
	}

	protected SADomain generateDomain() {
		SADomain domain = new SADomain();

		domain.addActionTypes(
				new UniversalActionType(ACTION_TAKE_STEP),
				new UniversalActionType(ACTION_TURN_RIGHT),
				new UniversalActionType(ACTION_TURN_LEFT));

		GridWorldStateModel smodel = new GridWorldStateModel();
		RewardFunction rf = new OfficeRF(this.goalx, this.goaly);
		TerminalFunction tf = new OfficeTF(this.goalx, this.goaly);

		domain.setModel(new FactoredModel(smodel, rf, tf));
		return domain;
	}

	protected Visualizer getVisualizer(){
		return new Visualizer(this.getStateRenderLayer());
	}

	private StateRenderLayer getStateRenderLayer(){
		StateRenderLayer rl = new StateRenderLayer();
		rl.addStatePainter(new GridWorldVisualizer.MapPainter(map));
		rl.addStatePainter(new HighObservationPainter());
		rl.addStatePainter(new GoalPainter());
		rl.addStatePainter(new AgentPainter());
		return rl;
	}

	private static int[] randomStartPosition(int[][] map) {
		// maxXCoordinate is to prevent the agent from starting really close from the goal
		int maxXCoordinate = 30;
		int x = rand.nextInt(maxXCoordinate);
		int y = rand.nextInt(map[0].length);
		int orientation = rand.nextInt(4);
		while(map[x][y] == 1 || map[x][y] == 9){
			x = rand.nextInt(maxXCoordinate);
			y = rand.nextInt(map[0].length);
		}
		return new int[]{x,y,orientation};
	}
	
	
	private class GridWorldStateModel implements FullStateModel{
		private double [][] transitionProbs;
		
		public GridWorldStateModel() {
			this.transitionProbs = new double[nActions][nActions];
			for(int i = 0; i < nActions; i++){
				for(int j = 0; j < nActions; j++){
					double p = i != j ? 0 : 1;
					transitionProbs[i][j] = p;
				}
			}
		}
		
		public List<StateTransitionProb> stateTransitions(State currState, Action a) {
			//get agent current position
			OfficeGridState newState = (OfficeGridState)currState;
			int curX = (Integer) newState.get(VAR_X);
			int curY = (Integer) newState.get(VAR_Y);
			
			List<StateTransitionProb> tps = new ArrayList<StateTransitionProb>(1);
			OfficeGridState ns = newState.copy();
			
			// A new state rotated to the right
			if(a.actionName().equals(ACTION_TURN_RIGHT)){
				ns.set(VAR_ORIENTATION, turnResult((Integer) ns.get(VAR_ORIENTATION), 1));
				tps.add(new StateTransitionProb(ns, 1));
			}
			// A new state rotated to the left
			else if(a.actionName().equals(ACTION_TURN_LEFT)){
				ns.set(VAR_ORIENTATION, turnResult((Integer) ns.get(VAR_ORIENTATION), -1));
				tps.add(new StateTransitionProb(ns, 1));
			}else if(a.actionName().equals(ACTION_TAKE_STEP)){
				int [] newPos = this.moveResult(curX, curY, (Integer)currState.get(VAR_ORIENTATION));
				if(newPos[0] != curX || newPos[1] != curY){
					ns.set(VAR_X, newPos[0]);
					ns.set(VAR_Y, newPos[1]);
				}
				tps.add(new StateTransitionProb(ns, 1));
			}
			return tps;
		}
		
		public State sample(State currState, Action a) {
			currState = currState.copy();
			OfficeGridState newState = (OfficeGridState)currState;
			int curX = (Integer) newState.get(VAR_X);
			int curY = (Integer) newState.get(VAR_Y);
			int orientation = (Integer)newState.get(VAR_ORIENTATION);

			if(a.actionName().equals(ACTION_TAKE_STEP)){
				int[] newPos = moveResult(curX, curY, orientation);
				newState.set(VAR_X, newPos[0]);
				newState.set(VAR_Y, newPos[1]);
			}else if(a.actionName().equals(ACTION_TURN_RIGHT)){
				newState.set(VAR_ORIENTATION, turnResult((Integer)newState.get(VAR_ORIENTATION), 1));
			}else if( a.actionName().equals(ACTION_TURN_LEFT)){
				newState.set(VAR_ORIENTATION, turnResult((Integer)newState.get(VAR_ORIENTATION), -1));
			}
			// XXX is this the right place to update araqv?
			aravq.addInput(newState.getDistanceSensorData());
			// XXX UNCOMMENT FOR SENSOR DATA INFO
//			System.out.println("[forward: " + sensorData[0] + ", right: " + sensorData[1]
//					 + ", backward: " + sensorData[2] + ", left: " + sensorData[3]);
			return newState;
		}
		
		public int [] moveResult(int curX, int curY, int direction){
			// 0 = north, 1 = east; 2 = south, 3 = west
			int xdelta = 0;
			int ydelta = 0;
			if(direction == 0){
				ydelta = 1;
			}
			else if(direction == 1){
				xdelta = 1;
			}
			else if(direction == 2){
				ydelta = -1;
			}
			else if (direction == 3){
				xdelta = -1;
			}else{
				return new int[]{0,0};
			}

			int nx = curX + xdelta;
			int ny = curY + ydelta;
			int width = OfficeGridWorld.this.map.length;
			int height = OfficeGridWorld.this.map[0].length;

			//make sure new position is valid (not off bounds or a wall)
			if(nx < 0 || nx >= width || ny < 0 || ny >= height || isWall(nx,ny)){
				nx = curX;
				ny = curY;
			}
			
			// if we stepped into a door, let's move one more time in the same direction
			if(OfficeGridWorld.this.map[nx][ny] == 9){
				return moveResult(nx, ny, direction);
			}
			return new int[]{nx,ny};
		}
		
		// turn_right -> dir = 1; turn_left -> dir = -1
		private int turnResult(int currOrientation, int dir){
			int newOrientation = currOrientation + dir;
			if(newOrientation > 3){
				newOrientation = 0;
			}else if(newOrientation < 0){
				newOrientation = 3;
			}
			return newOrientation;
		}
		
		private boolean isWall(int x, int y){
			System.out.println("ola");
			return OfficeGridWorld.this.map[x][y] == 1;
		}
	}
	
	// INNER CLASS
		public static class OfficeTF implements TerminalFunction {
			int goalX;
			int goalY;

			public OfficeTF(int goalX, int goalY){
				this.goalX = goalX;
				this.goalY = goalY;
			}

			public boolean isTerminal(State s) {
				//get location of agent in next state
				int x = (Integer)s.get(VAR_X);
				int y = (Integer)s.get(VAR_Y);
				return x == this.goalX && y == this.goalY;
			}
		}
	
	// INNER CLASS
	public static class OfficeRF implements RewardFunction {
		int goalX;
		int goalY;

		public OfficeRF(int goalX, int goalY){
			this.goalX = goalX;
			this.goalY = goalY;
		}

		public double reward(State s, Action a, State sprime) {
			int ax = (Integer)sprime.get(VAR_X);
			int ay = (Integer)sprime.get(VAR_Y);

			//are they at goal location?
			if(ax == this.goalX && ay == this.goalY){
				return 20;
			}
			return -1;
		}
	}
	
	// INNER CLASS
	public class GoalPainter implements StatePainter {
		public void paint(Graphics2D g2, State s, float cWidth, float cHeight) {
			float fWidth = map.length;
			float fHeight = map[0].length;
			float width = cWidth / fWidth;
			float height = cHeight / fHeight;
	
			// paint the goal
			int ry = (int)(cHeight - height - goaly * height);
			int rx = (int)(goalx * width);
			g2.setColor(Color.RED);
			g2.fillRect(rx, ry, (int) width, (int) height);
		}
	}
	
	
	// INNER CLASS
	public class AgentPainter implements StatePainter {
		public void paint(Graphics2D g2, State s,
								float cWidth, float cHeight) {
			//agent will be filled in gray
			g2.setColor(Color.GRAY);
	
			//set up floats for the width and height of our domain
			float fWidth = map.length;
			float fHeight = map[0].length;
	
			//determine the width of a single cell on our canvas
			//such that the whole map can be painted
			float width = cWidth / fWidth;
			float height = cHeight / fHeight;
	
			int ax = (Integer) s.get(VAR_X);
			int ay = (Integer) s.get(VAR_Y);
	
			//left coordinate of cell on our canvas
			float rx = ax*width;
	
			//top coordinate of cell on our canvas
			//coordinate system adjustment because the java canvas
			//origin is in the top left instead of the bottom right
			float ry = cHeight - height - ay*height;
	
			//paint the rectangle
			// 0 = north, 1 = east; 2 = south, 3 = west
			int orientation = (Integer) s.get(VAR_ORIENTATION);
			if(orientation == 0){
				g2.drawLine((int)(rx + width/2), (int) ry, (int)(rx + width/2), (int)ry - 3);
			}else if(orientation == 1){
				g2.drawLine((int)(rx + width), (int)(ry + height/2), (int)(rx + width) + 3, (int)(ry + height/2));
			}else if(orientation == 2){
				g2.drawLine((int)(rx + width/2), (int) (ry + height), (int)(rx + width/2), (int)(ry + height) + 3);
			}else if(orientation == 3){
				g2.drawLine((int)rx, (int)(ry + height/2), (int)rx - 3, (int)(ry + height/2));
			}
			g2.fill(new Ellipse2D.Float(rx, ry, width, height));
		}
	}
	
	// INNER CLASS
		public class HighObservationPainter implements StatePainter {
			OfficeGridState shellState = new OfficeGridState(map, 1, 1, 0);
			public void paint(Graphics2D g2, State s, float cWidth, float cHeight) {
				//determine then normalized width
				float width = (1.0f / map.length) * cWidth;
				float height = (1.0f / map[0].length) * cHeight;
				
				for(int i = 0;i < map.length; i++){
					for(int j = 0; j < map[0].length; j++){
						if(map[i][j] == 0){
							float rx = (Integer)i*width;
							float ry = cHeight - height - (Integer)j*height;
							int vectNumb = aravq.pickWinningVector(shellState.distanceSensorsAt(i,j,1));
							if(vectNumb == -1)
								vectNumb = 1;
							g2.setColor(getObservationColor(vectNumb));
							g2.fill(new Rectangle2D.Float(rx, ry, width, height));
//							g2.drawString(String.valueOf(vectNumb), rx, ry); // TODO
						}
					}
				}
			}
			
			private Color getObservationColor(int numb) {
				if(numb == 0){
					return new Color(40,10,10,80);			
				}else if(numb == 1){
					return new Color(80,15,15,80);
				}else if(numb == 2){
					return new Color(180,138,109,80);
				}else if(numb == 3){
					return new Color(141,248,23,80);
				}else if(numb == 4){
					return new Color(240,213,131,80);
				}else if(numb == 5){
					return new Color(51,103,172,80);
				}else if(numb == 6){
					return new Color(18,21,12,80);
				}else if(numb == 7){
					return new Color(175,45,230,80);
				}else if(numb == 8){
					return new Color(43,248,126,80);
				}else if(numb == 9){
					return new Color(68,225,76,80);
				}else if(numb == 10){
					return new Color(25,150,19,80);
				}else if(numb == 11){
					return new Color( 32,197,178,80);
				}else if(numb == 12){
					return new Color(230,92,204,80);
				}else if(numb == 13){
					return new Color(138,168,162,80);
				}else if(numb == 15){
					return new Color(203,227,54,80);
				}else if(numb == 16){
					return new Color(169,105,123,80);
				}else{
					return Color.BLACK;
				}
			}
		}
}