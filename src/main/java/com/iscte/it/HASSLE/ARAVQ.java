package com.iscte.it.HASSLE;

import java.util.ArrayList;
import java.util.List;

import EDU.oswego.cs.dl.util.concurrent.misc.PipedChannel;

@SuppressWarnings("unused")
public class ARAVQ {
	private	final double 		learningRate;		// Not used for HASSLE
	private	final double 		stabilityCriterion; // Big number for HASSLE??
	private	final double 		noveltyCriterion;
	private	final int 			bufferSize;
	
	private final int[]			movingAverage;
	private final List<int[]> 	inputBuffer;
	private final List<int[]>	modelVectors;
	
	public ARAVQ(double learningRate, double stabilityCriterion, double noveltyCriterion, int inputSize, int bufferSize) {
		this.learningRate = learningRate;
		this.stabilityCriterion = stabilityCriterion;
		this.noveltyCriterion = noveltyCriterion;
		this.bufferSize = bufferSize;
		
		this.movingAverage = new int[inputSize];
		this.inputBuffer = new ArrayList<int[]>();
		this.modelVectors = new ArrayList<int[]>();
	}
	
	public double distanceBetweenVectorSets(int[][] setOne, int[][] setTwo){
		// If the modelVector list is empty this will assure a new model vector is added to it by returning a high
		// enough result
		if(setOne.length == 0 || setTwo.length == 0){
			return noveltyCriterion + stabilityCriterion;
		}
		double result = 0;

		for(int iteratorSetTwo = 0; iteratorSetTwo < setTwo.length; iteratorSetTwo++){
			double min = Integer.MAX_VALUE;
			for(int iteratorSetOne = 0; iteratorSetOne < setOne.length; iteratorSetOne++){
				double euclideanDist = calculateEuclideanDist(setOne[iteratorSetOne], setTwo[iteratorSetTwo]);
				min = euclideanDist < min ? euclideanDist : min;
			}
			result += min;
		}
		return result /setTwo.length;
	}

	public double calculateEuclideanDist(int[] vectorOne, int[] vectorTwo) {
		if(vectorOne.length != vectorTwo.length){
			throw new IllegalStateException("Vector one (size = " + vectorOne.length + ") and vector two (size = "
					+ vectorTwo.length + ") must have the same size.");
		}
		
		int result = 0;
		for(int i = 0; i < vectorOne.length; i++){
			result += (vectorOne[i] - vectorTwo[i])*(vectorOne[i] - vectorTwo[i]);
		}
		return Math.sqrt(result); 
	}
	
	public int[][] listToMatrix(List<int[]> list){
		int[][] mVectors = new int[list.size()][];
		list.toArray(mVectors);
		return mVectors;
	}
	
	public boolean isNovel(){
		int[][] input = listToMatrix(inputBuffer);
		double mismatchInputAverage = distanceBetweenVectorSets(new int[][]{movingAverage}, input);
		double mismatchInputModel = distanceBetweenVectorSets(listToMatrix(modelVectors), input);
		return mismatchInputModel - mismatchInputAverage >= noveltyCriterion;
	}
	
	public boolean isStable(){
		int[][] input = listToMatrix(inputBuffer);
		return distanceBetweenVectorSets(new int[][]{movingAverage}, input) <= stabilityCriterion;
	}
	
	public void calculateMovingAverage(){
		int accumulator = 0;
		for (int i = 0; i < movingAverage.length; i++){
			for(int j = 0; j < bufferSize; j++){
				accumulator += inputBuffer.get(j)[i];
			}
			movingAverage[i] = accumulator / bufferSize;
			accumulator = 0;
		}
	}
	
	public void addInput(int[] input){
		inputBuffer.add(input);
		if(inputBuffer.size() == bufferSize){
			processInputBuffer();
			inputBuffer.clear();
		}
	}
	
	public void processInputBuffer(){
		calculateMovingAverage();
		if(isNovel() && isStable()){
			modelVectors.add(movingAverage.clone());
		}
	}
	
	public int pickWinningVector(int[] sampleVector){
		int index = -1;
		double min = Integer.MAX_VALUE;
		double euclDist = 0;
		for(int i = 0; i < modelVectors.size(); i++){
			euclDist = calculateEuclideanDist(modelVectors.get(i), sampleVector);
			if(euclDist < min){
				min = euclDist;
				index = i;
			}
		}
		return index;
	}
	
	public void adaptation(){
		// TODO not used for HASSLE
	}
	
	public List<int[]> getModelVectors(){
		return modelVectors;
	}
	
	public static void printVector(int[] v){
		System.out.print("[");
		for(int j = 0; j < v.length; j++){
			System.out.print(" " + v[j] + " ");
		}
		System.out.print("]\n");
	}
	
	public static void main(String[] args) {
		ARAVQ aravq = new ARAVQ(0, 0, 0, 3, 2);
		aravq.addToBuffer(new int[]{10,5,3}, new int[]{5,2,2});
		aravq.calculateMovingAverage();
		printVector(aravq.getMovingAverage());
	}

	private int[] getMovingAverage() {
		return movingAverage;
	}

	private void addToBuffer(int[] is, int[] is2) {
		inputBuffer.add(is);
		inputBuffer.add(is2);
	}
}