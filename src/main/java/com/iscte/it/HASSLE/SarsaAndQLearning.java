package com.iscte.it.HASSLE;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.util.List;

import burlap.behavior.singleagent.Episode;
import burlap.behavior.singleagent.auxiliary.EpisodeSequenceVisualizer;
import burlap.behavior.singleagent.learning.LearningAgent;
import burlap.behavior.singleagent.learning.tdmethods.QLearning;
import burlap.behavior.singleagent.learning.tdmethods.SarsaLam;
import burlap.mdp.core.state.State;
import burlap.mdp.singleagent.SADomain;
import burlap.mdp.singleagent.environment.SimulatedEnvironment;
import burlap.statehashing.HashableStateFactory;
import burlap.statehashing.simple.SimpleHashableStateFactory;
import burlap.visualizer.Visualizer;

public class SarsaAndQLearning {
	OfficeGridWorld officeGirdWorld;
	SADomain domain;
	State initialState;
	HashableStateFactory hashingFactory;
	SimulatedEnvironment env;
	private static final int NUMB_EPISODES = 10000;

	public static void main(String[] args) {
		SarsaAndQLearning example = new SarsaAndQLearning();
//		String outputPath = "C:/Users/André Freire/Downloads/Faculdade/Research/HASSLE/showcaseOutput/"; // directory to visualize results
		String outputPath = "C:/Users/André Freire/Downloads/Faculdade/Research/HASSLE/output/"; // directory to record results

		// we will call planning and learning algorithms here
		example.QLearningExample(outputPath);
//		example.SarsaLearningExample(outputPath);

		// run the visualizer
		example.visualize(outputPath);
	}

	public SarsaAndQLearning(){
			officeGirdWorld = new OfficeGridWorld();
			domain = officeGirdWorld.generateDomain();
			initialState = OfficeGridWorld.randomInitialState(officeGirdWorld.getMap());
			env = new SimulatedEnvironment(domain, initialState);
			hashingFactory = new SimpleHashableStateFactory();
	}

	public void visualize(String outputPath) {
		Visualizer v = officeGirdWorld.getVisualizer();
		new EpisodeSequenceVisualizer(v, domain, outputPath);
	}

	public void QLearningExample(String outputPath) {
		if(outputPath.contains("showcaseOutput")){
			throw new IllegalStateException("Trying to save results to the showcase folder. Change the outputPath variable.");
		}
		LearningAgent agent = new QLearning(domain, 0.99, hashingFactory, 0., 1.);
		String timeStepsPerEpisode = new String();
		String lineSeparator = System.getProperty("line.separator");
		int lowLevelSteps = 0;
		// run learning for NUMB_EPISODES episodes
		for (int i = 0; i < NUMB_EPISODES; i++) {
			env.setCurStateTo(OfficeGridWorld.randomInitialState(officeGirdWorld.getMap()));
			Episode e = agent.runLearningEpisode(env);
			
			// write every 100 episodes to a file
			if(i % 100 == 0){
				e.write(outputPath + "ql_" + i);
			}
			lowLevelSteps += e.numTimeSteps();
			timeStepsPerEpisode += i + ": " + e.maxTimeStep() + lineSeparator;
			env.resetEnvironment();
		}

		timeStepsPerEpisode = "Total Steps : " + lowLevelSteps + lineSeparator + timeStepsPerEpisode;
		writeTimeStepsPerEpisodeToFile(timeStepsPerEpisode,outputPath + "q_timeStepsPerEpisode.txt");
		 // XXX remove debug messages
		List<int[]> highObs = officeGirdWorld.getHighObservations();
		System.out.println(highObs.size());
		for(int i = 0; i < highObs.size(); i++){
			System.out.print("Vector "+ i + ": ");
			ARAVQ.printVector(highObs.get(i));
		}
	}

	public void SarsaLearningExample(String outputPath) {
		if(outputPath.contains("showcaseOutput")){
			throw new IllegalStateException("Trying to save results to the showcase folder. Change the outputPath variable.");
		}
		LearningAgent agent = new SarsaLam(domain, 0.99, hashingFactory, 0., 0.5, 0.3);
		// run learning for NUMB_EPISODES episodes
		for (int i = 0; i < NUMB_EPISODES; i++) {
			Episode e = agent.runLearningEpisode(env);

			e.write(outputPath + "sarsa_" + i);
			System.out.println(i + ": " + e.maxTimeStep());

			// reset environment for next learning episode
			env.resetEnvironment();
		}
	}
	
	private void writeTimeStepsPerEpisodeToFile(String timeStepsPerEpisode, String path){
		File f = (new File(path)).getParentFile();
		if(f != null){
			f.mkdirs();
		}
		try{
			BufferedWriter out = new BufferedWriter(new FileWriter(path));
			out.write(timeStepsPerEpisode);
			out.close();
		}catch(Exception e){
			System.out.println(e);
		}
	}
}