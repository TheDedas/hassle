package com.iscte.it.HASSLE;

import java.util.Arrays;
import java.util.List;
import burlap.mdp.core.state.MutableState;
import burlap.mdp.core.state.StateUtilities;
import burlap.mdp.core.state.UnknownKeyException;
import static com.iscte.it.HASSLE.OfficeGridWorld.*;

public class OfficeGridState implements MutableState {
	public int[][] map;
	public int x;
	public int y;
	public int orientation;// 0 = north, 1 = east; 2 = south, 3 = west
	
	// Sensor data
	// Distance from wall/door in grid cells (relative to the current
	// orientation)
	public int distForward;
	public int distRight;
	public int distBackward;
	public int distLeft;
	// Presence of doors ( 0 = not present, 1 = present)
	public int isDoorForward;
	public int isDoorRight;
	public int isDoorBackward;
	public int isDoorLeft;
	// Presence of the goal ( 0 = not present, 1 = present) within a distance of
	// 8 cells
	public static final int GOAL_DETECTION_MAX_DIST = 8;
	public int isGoalForward;
	public int isGoalRight;
	public int isGoalBackward;
	public int isGoalLeft;

	public final static List<Object> keys = Arrays.<Object> asList(VAR_X, VAR_Y, VAR_ORIENTATION, VAR_DIST_FORWARD,
			VAR_DIST_RIGHT, VAR_DIST_BACKWARD, VAR_DIST_LEFT, VAR_IS_DOOR_FORWARD, VAR_IS_DOOR_RIGHT,
			VAR_IS_DOOR_BACKWARD, VAR_IS_DOOR_LEFT, VAR_IS_GOAL_FORWARD, VAR_IS_GOAL_RIGHT, VAR_IS_GOAL_BACKWARD,
			VAR_IS_GOAL_LEFT);

	public OfficeGridState() {
	}

	public OfficeGridState(int[][] map, int x, int y, int orientation) {
		this.map = map;
		this.x = x;
		this.y = y;
		this.orientation = orientation;
		updateSensors(distanceSensorsAt(x, y, orientation));
	}
	
	public List<Object> variableKeys() {
		return keys;
	}

	public Object get(Object variableKey) {
		if (variableKey.equals(VAR_X)) {
			return x;
		} else if (variableKey.equals(VAR_Y)) {
			return y;
		} else if (variableKey.equals(VAR_ORIENTATION)) {
			return orientation;
		} else if (variableKey.equals(VAR_DIST_FORWARD)) {
			return distForward;
		} else if (variableKey.equals(VAR_DIST_RIGHT)) {
			return distRight;
		} else if (variableKey.equals(VAR_DIST_BACKWARD)) {
			return distBackward;
		} else if (variableKey.equals(VAR_DIST_LEFT)) {
			return distLeft;
		} else if (variableKey.equals(VAR_IS_DOOR_FORWARD)) {
			return isDoorForward;
		} else if (variableKey.equals(VAR_IS_DOOR_RIGHT)) {
			return isDoorRight;
		} else if (variableKey.equals(VAR_IS_DOOR_BACKWARD)) {
			return isDoorBackward;
		} else if (variableKey.equals(VAR_IS_DOOR_LEFT)) {
			return isDoorLeft;
		} else if (variableKey.equals(VAR_IS_GOAL_FORWARD)) {
			return isGoalForward;
		} else if (variableKey.equals(VAR_IS_GOAL_RIGHT)) {
			return isGoalRight;
		} else if (variableKey.equals(VAR_IS_GOAL_BACKWARD)) {
			return isGoalBackward;
		} else if (variableKey.equals(VAR_IS_GOAL_LEFT)) {
			return isGoalLeft;
		}
		throw new UnknownKeyException(variableKey);
	}

	public OfficeGridState copy() {
		return new OfficeGridState(map, x, y, orientation);
	}

	public MutableState set(Object variableKey, Object value) {
		if (variableKey.equals(VAR_X)) {
			this.x = StateUtilities.stringOrNumber(value).intValue();
		} else if (variableKey.equals(VAR_Y)) {
			this.y = StateUtilities.stringOrNumber(value).intValue();
		} else if (variableKey.equals(VAR_ORIENTATION)) {
			this.orientation = StateUtilities.stringOrNumber(value).intValue();
		} else if (variableKey.equals(VAR_DIST_FORWARD)) {
			this.distForward = StateUtilities.stringOrNumber(value).intValue();
		} else if (variableKey.equals(VAR_DIST_RIGHT)) {
			this.distRight = StateUtilities.stringOrNumber(value).intValue();
		} else if (variableKey.equals(VAR_DIST_BACKWARD)) {
			this.distBackward = StateUtilities.stringOrNumber(value).intValue();
		} else if (variableKey.equals(VAR_DIST_LEFT)) {
			this.distLeft = StateUtilities.stringOrNumber(value).intValue();
		} else if (variableKey.equals(VAR_IS_DOOR_FORWARD)) {
			this.isDoorForward = StateUtilities.stringOrNumber(value).intValue();
		} else if (variableKey.equals(VAR_IS_DOOR_RIGHT)) {
			this.isDoorRight = StateUtilities.stringOrNumber(value).intValue();
		} else if (variableKey.equals(VAR_IS_DOOR_BACKWARD)) {
			this.isDoorBackward = StateUtilities.stringOrNumber(value).intValue();
		} else if (variableKey.equals(VAR_IS_DOOR_LEFT)) {
			this.isDoorLeft = StateUtilities.stringOrNumber(value).intValue();
		} else if (variableKey.equals(VAR_IS_GOAL_FORWARD)) {
			this.isGoalForward = StateUtilities.stringOrNumber(value).intValue();
		} else if (variableKey.equals(VAR_IS_GOAL_RIGHT)) {
			this.isGoalRight = StateUtilities.stringOrNumber(value).intValue();
		} else if (variableKey.equals(VAR_IS_GOAL_BACKWARD)) {
			this.isGoalBackward = StateUtilities.stringOrNumber(value).intValue();
		} else if (variableKey.equals(VAR_IS_GOAL_LEFT)) {
			this.isGoalLeft = StateUtilities.stringOrNumber(value).intValue();
		} else {
			throw new UnknownKeyException(variableKey);
		}
		updateSensors(distanceSensorsAt(x,y, orientation));
		return this;
	}

	// TODO needs to be finished
	public int[] distanceSensorsAt(int xCord, int yCord, int orientation) {
		int initialOffset = 0;
		int offset = initialOffset;
		if(orientation == 0){// 0 = north
			for(offset = initialOffset; map[xCord][yCord + offset] == 0;offset++){}
			distForward = offset;
			for(offset = initialOffset; map[xCord + offset][y] == 0;offset++){}
			distRight = offset;
			for(offset = initialOffset; map[xCord][yCord - offset] == 0;offset++){}
			distBackward = offset;
			for(offset = initialOffset; map[xCord - offset][yCord] == 0;offset++){}
			distLeft = offset;
		}else if(orientation == 1){// 1 = east
			for(offset = initialOffset; map[xCord + offset][yCord] == 0;offset++){}
			distForward = offset;
			for(offset = initialOffset; map[xCord][yCord - offset] == 0;offset++){}
			distRight = offset;
			for(offset = initialOffset; map[xCord - offset][yCord] == 0;offset++){}
			distBackward = offset;
			for(offset = initialOffset; map[xCord][yCord + offset] == 0;offset++){}
			distLeft = offset;
		}else if(orientation == 2){// 2 = south
			for(offset = initialOffset; map[xCord][yCord - offset] == 0;offset++){}
			distForward = offset;
			for(offset = initialOffset; map[xCord - offset][yCord] == 0;offset++){}
			distRight = offset;
			for(offset = initialOffset; map[xCord][yCord + offset] == 0;offset++){}
			distBackward = offset;
			for(offset = initialOffset; map[xCord + offset][yCord] == 0;offset++){}
			distLeft = offset;
		}else if(orientation == 3){// 3 = west 
			for(offset = initialOffset; map[xCord - offset][yCord] == 0;offset++){}
			distForward = offset;
			for(offset = initialOffset; map[xCord][yCord + offset] == 0;offset++){}
			distRight = offset;
			for(offset = initialOffset; map[xCord + offset][yCord] == 0;offset++){}
			distBackward = offset;
			for(offset = initialOffset; map[xCord][yCord - offset] == 0;offset++){}
			distLeft = offset;
		}
		return getDistanceSensorData();
	}

	public void updateSensors(int[] sensorData) {
		distForward = sensorData[0];
		distRight = sensorData[1];
		distBackward = sensorData[2];
		distLeft = sensorData[3];
	}

	
	public int[] getDistanceSensorData() {
		return new int[]{distForward,distRight,distBackward, distLeft};
	}
}